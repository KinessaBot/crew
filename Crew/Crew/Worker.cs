﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Crew
{
    class Worker: IComparer
    {
        public string Name { get; set; }
        public string WorkingPosition { get; set; }
        public int Compare(object x, object y)
        {
            return string.Compare(((Worker)x).WorkingPosition, ((Worker)y).WorkingPosition);
        }

        public Worker(string name, string workingPosition)
        {
            Name = name;
            WorkingPosition = workingPosition;
        }
    }
}
