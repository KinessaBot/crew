﻿using System;

namespace Crew
{
    class Program
    {
        static void Main(string[] args)
        {
            Crew crew = new Crew();
            crew.Add((object)new Worker("John Johnson", "Library"));
            crew.Add((object)new Worker("Will Johnson", "Main office"));
            crew.Add((object)new Worker("Sam Parenton", "Illusiana office"));

            object maxObject = (object) new Worker("Kate Bannenhoff","Illusiana Reception");
            crew.Insert(2, maxObject);
            foreach (object obj in crew)
            {
                if (((Worker)obj).Compare(obj,maxObject)>1)
                    maxObject = obj;
            }
            Console.WriteLine("Maximal working place is " + ((Worker)maxObject).WorkingPosition);
           
            crew.Remove(maxObject);
            crew.RemoveAt(crew.IndexOf(crew[2]));

            foreach (object obj in crew)
                Console.WriteLine(((Worker)obj).Name + " " + ((Worker)obj).WorkingPosition);

            Console.ReadLine();
        }
    }
}
