﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Crew
{
    class Crew : IList
    {
        List<Worker> list = new List<Worker>();
        public object this[int index] { get
            {
                return list[index];
            }
            set
            {
                list[index] = (Worker)value;
            }
 }

        public bool IsFixedSize => throw new NotImplementedException();

        public bool IsReadOnly => throw new NotImplementedException();

        public int Count { get { return list.Count; } }

        public bool IsSynchronized => throw new NotImplementedException();

        public object SyncRoot => throw new NotImplementedException();

        public int Add(object value)
        {
            list.Add((Worker)value);
            return 0;
        }

        public void Clear()
        {
            list.Clear();
        }

        public bool Contains(object value)
        {
            return list.Contains((Worker)value);
        }

        public void CopyTo(Array array, int index)
        {
            list.CopyTo((Worker[])array, index);
        }

        public IEnumerator GetEnumerator()
        {
            return list.GetEnumerator();
        }

        public int IndexOf(object value)
        {
            return list.IndexOf((Worker)value);
        }

        public void Insert(int index, object value)
        {
            list.Insert(index, (Worker)value);
        }

        public void Remove(object value)
        {
            list.Remove((Worker)value);
        }

        public void RemoveAt(int index)
        {
            list.RemoveAt(index);
        }
    }
}
